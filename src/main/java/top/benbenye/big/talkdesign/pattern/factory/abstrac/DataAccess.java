package top.benbenye.big.talkdesign.pattern.factory.abstrac;

/**
 * @author Li Zemin
 * @since 2024/6/7 14:31
 */
public class DataAccess {

	private static final String AssemblyName = "top.benbenye.big.talkdesign.pattern.abstrac.factory";
	private static final String db = "MySQL";

	public static IUser CreateUser(){
		try {
			Class<?> aClass = ClassLoader.getSystemClassLoader().loadClass(AssemblyName + "." + db + "User");
			IUser o = (IUser)aClass.newInstance();
			return o;
		} catch (ClassNotFoundException e) {
			throw new RuntimeException(e);
		} catch (InstantiationException e) {
			throw new RuntimeException(e);
		} catch (IllegalAccessException e) {
			throw new RuntimeException(e);
		}

	}

}
