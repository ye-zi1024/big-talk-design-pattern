package top.benbenye.big.talkdesign.pattern.factory.abstrac;


/**
 * @author Li Zemin
 * @since 2024/6/7 14:40
 */
public class MySQLUser implements IUser{
	@Override
	public void insert(User user) {
		System.out.println("insert MySQLUser");
	}

	@Override
	public User getUser(int id) {
		return new User();
	}
}
