package top.benbenye.big.talkdesign.pattern.composite;

/**
 * @author Li Zemin
 * @since 2024/6/11 11:12
 */
public class Client {

	public static void main(String[] args) {

		Composite root = new Composite("root");
		root.add(new Leaf("Leaf A"));
		root.add(new Leaf("Leaf B"));

		Composite comp = new Composite("Composite X");
		comp.add(new Leaf("Leaf XA"));
		comp.add(new Leaf("Leaf XB"));
		root.add(comp);

		Composite comp2 = new Composite("Composite XY");
		comp2.add(new Leaf("Leaf XYA"));
		comp2.add(new Leaf("Leaf XYB"));
		root.add(comp2);

		root.add(new Leaf("Leaf C"));

		Leaf leaf_d = new Leaf("Leaf D");
		root.add(leaf_d);
		root.remove(leaf_d);

		root.display(1);

	}
}
