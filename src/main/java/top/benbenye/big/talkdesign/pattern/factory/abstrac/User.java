package top.benbenye.big.talkdesign.pattern.factory.abstrac;

/**
 * @author Li Zemin
 * @since 2024/6/7 14:41
 */
public class User {
	private int id;
	private String name;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}
