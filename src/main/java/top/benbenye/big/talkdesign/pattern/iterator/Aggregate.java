package top.benbenye.big.talkdesign.pattern.iterator;

/**
 * 聚合抽象类.
 *
 * @author Li Zemin
 * @since 2024/6/12 9:04
 */
public abstract class Aggregate {

	public abstract Iterator createIterator();
}
