package top.benbenye.big.talkdesign.pattern.singleton;

/**
 * @author Li Zemin
 * @since 2024/6/12 10:40
 */
public class Singleton {

	private static Singleton singleton = null;

	private Singleton() {
	}

	public static Singleton getInstance() {
		if (singleton == null) {
			singleton = new Singleton();
		}
		return singleton;
	}
}
