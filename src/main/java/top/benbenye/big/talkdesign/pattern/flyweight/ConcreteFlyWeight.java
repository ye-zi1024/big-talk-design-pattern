package top.benbenye.big.talkdesign.pattern.flyweight;

public class ConcreteFlyWeight extends FlyWeight{

  @Override
  public void operation(int extrinsicstate) {
    System.out.println("具体fly-weight：" + extrinsicstate);
  }
}
