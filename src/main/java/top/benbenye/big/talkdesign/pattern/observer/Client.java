package top.benbenye.big.talkdesign.pattern.observer;

/**
 * @author Li Zemin
 * @since 2024/5/31 11:35
 */
public class Client {
	public static void main(String[] args) {
		ConcreteSubject s = new ConcreteSubject();
		s.attach(new ConcreteObserver("X", s));
		s.attach(new ConcreteObserver("Y", s));
		s.attach(new ConcreteObserver("Z", s));

		s.setSubjectState("ABC");
		s.notifys();
	}
}
