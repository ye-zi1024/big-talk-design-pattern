package top.benbenye.big.talkdesign.pattern.prototype;

/**
 * @author Li Zemin
 * @since 2024/5/24 14:53
 */
public class Resume implements Cloneable {

	private String name;
	private String sex;
	private String age;

	private WorkExperience workExperience;

	public Resume(String name) {
		this.name = name;
		this.workExperience = new WorkExperience();
	}

	public Resume(WorkExperience workExperience) {
		this.workExperience = workExperience.clone();
	}

	public void SetPersonalInfo(String sex, String age) {
		this.sex = sex;
		this.age = age;
	}

	public void SetWorkExperience(String workDate, String company) {
		this.workExperience.setWorkDate(workDate);
		this.workExperience.setCompany(company);
	}

	public void display() {
		System.out.println("Resume{" +
				"name='" + name + '\'' +
				", sex='" + sex + '\'' +
				", age='" + age + '\'' +
				", workDate='" + workExperience.getWorkDate() + '\'' +
				", company='" + workExperience.getCompany() + '\'' +
				'}');
	}


	/**
	 * 浅复制
	 * 值类型复制没有任何问题，对于引用类型来说，只是复制了引用，引用对象还是指向了原来的对象
	 * @return
	 */
	@Override
	public Resume clone() {
		try {
			// TODO: copy mutable state here, so the clone can't change the internals of the original
//			Resume clone = (Resume) super.clone();

			Resume resume = new Resume(this.workExperience);
			resume.name = this.name;
			resume.sex = this.sex;
			resume.age = this.age;
			return resume;

		} catch (Exception ex) {
			throw new AssertionError();
		}
	}
}
