package top.benbenye.big.talkdesign.pattern.bridge;

/**
 * @author Li Zemin
 * @since 2024/6/12 15:27
 */
public abstract class Implementor {

	public abstract void operation();

}
