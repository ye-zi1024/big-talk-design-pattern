package top.benbenye.big.talkdesign.pattern.factory.method;

/**
 * @author Li Zemin
 * @since 2024/5/24 14:40
 */
public class VolunteerFactory implements IFactory{
	@Override
	public LeiFeng CreateLeiFeng() {
		return new Volunteer();
	}
}
