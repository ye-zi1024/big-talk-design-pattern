package top.benbenye.big.talkdesign.pattern.mediator;

/**
 * 抽象中介者.
 *
 * @author Li Zemin
 * @since 2024/6/12 17:09
 */
public abstract class Mediator {

	public abstract void send(String message, Colleague colleague);
}
