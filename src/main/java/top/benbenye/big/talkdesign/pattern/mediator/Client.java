package top.benbenye.big.talkdesign.pattern.mediator;

/**
 * @author Li Zemin
 * @since 2024/6/12 17:15
 */
public class Client {

	public static void main(String[] args) {
		ConcreteMediator concreteMediator = new ConcreteMediator();
		ConcreteColleague1 concreteColleague1 = new ConcreteColleague1(concreteMediator);
		ConcreteColleague2 concreteColleague2 = new ConcreteColleague2(concreteMediator);
		concreteMediator.setColleague1(concreteColleague1);
		concreteMediator.setColleague2(concreteColleague2);

		concreteColleague1.send("吃饭了么？");
		concreteColleague2.send("没有");
	}
}
