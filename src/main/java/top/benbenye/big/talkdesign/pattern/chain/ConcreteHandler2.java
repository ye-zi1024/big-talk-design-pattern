package top.benbenye.big.talkdesign.pattern.chain;

/**
 * @author Li Zemin
 * @since 2024/6/12 16:49
 */
public class ConcreteHandler2 extends Handler {
	@Override
	public void HandleRequest(int request) {
		if (request >= 10 && request < 20) {
			System.out.println(this.getType() + " 处理请求 " + request);
		} else if (successor != null) {
			successor.HandleRequest(request);
		}
	}

	private String getType() {
		return "李四";
	}
}
