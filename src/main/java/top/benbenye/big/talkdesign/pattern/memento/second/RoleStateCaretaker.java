package top.benbenye.big.talkdesign.pattern.memento.second;

import lombok.Data;

/**
 * 管理者.
 *
 * @author Li Zemin
 * @since 2024/6/11 10:50
 */
@Data
public class RoleStateCaretaker {

	private RoleStateMemento memento;

}
