package top.benbenye.big.talkdesign.pattern.state;

import lombok.Data;

/**
 * @author Li Zemin
 * @since 2024/6/7 15:12
 */
public class Context {
	private State state;

	public Context(State state) {
		this.state = state;
	}

	public State getState() {
		return state;
	}

	public void setState(State state) {
		this.state = state;
		System.out.println("当前状态：" + state.getClass().getName());
	}

	public void request(){
		state.handle(this);
	}

}
