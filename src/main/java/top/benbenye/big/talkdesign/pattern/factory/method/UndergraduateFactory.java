package top.benbenye.big.talkdesign.pattern.factory.method;

/**
 * @author Li Zemin
 * @since 2024/5/24 14:39
 */
public class UndergraduateFactory implements IFactory{
	@Override
	public LeiFeng CreateLeiFeng() {
		return new Undergraduate();
	}
}
