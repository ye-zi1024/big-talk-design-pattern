package top.benbenye.big.talkdesign.pattern.factory.method;

/**
 * @author Li Zemin
 * @since 2024/5/24 14:38
 */
public interface IFactory {

	LeiFeng CreateLeiFeng();
}
