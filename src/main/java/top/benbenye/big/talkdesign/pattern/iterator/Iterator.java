package top.benbenye.big.talkdesign.pattern.iterator;

/**
 * 迭代器抽象类.
 *
 * @author Li Zemin
 * @since 2024/6/12 9:03
 */
public abstract class Iterator {

	public abstract Object first();

	public abstract Object next();

	public abstract Boolean isdone();

	public abstract Object currentItem();

}
