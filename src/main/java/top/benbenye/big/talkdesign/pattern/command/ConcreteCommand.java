package top.benbenye.big.talkdesign.pattern.command;

/**
 * @author Li Zemin
 * @since 2024/6/12 15:52
 */
public class ConcreteCommand extends Command {

	public ConcreteCommand(Receiver receiver) {
		super(receiver);
	}

	@Override
	public void execute() {
		receiver.action();
	}
}
