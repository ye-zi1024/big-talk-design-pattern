package top.benbenye.big.talkdesign.pattern.chain;

/**
 * @author Li Zemin
 * @since 2024/6/12 16:49
 */
public class ConcreteHandler3 extends Handler {
	@Override
	public void HandleRequest(int request) {
		if (request >= 20 && request < 30) {
			System.out.println(this.getType() + " 处理请求 " + request);
		} else if (successor != null) {
			successor.HandleRequest(request);
		} else {
			System.out.println("谁也处理不了，毁灭吧 " + request);
		}
	}

	private String getType() {
		return "王五";
	}
}
