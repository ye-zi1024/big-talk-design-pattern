package top.benbenye.big.talkdesign.pattern.visitor;

public class ConcreteElementB extends Element {

  @Override
  public void accept(Visitor visitor) {
    visitor.VisitConcreteElementB(this);
  }

  public String getName() {
    return "ConcreteElementB";
  }
}
