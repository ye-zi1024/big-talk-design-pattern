package top.benbenye.big.talkdesign.pattern.factory.simple;

/**
 * @author Li Zemin
 * @since 2024/5/14 17:12
 */
public class OperationAdd extends Operation {

	@Override
	public double getResult() {
		return getNumberA() + getNumberB();
	}
}
