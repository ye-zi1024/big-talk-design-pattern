package top.benbenye.big.talkdesign.pattern.factory.simple;

/**
 * @author Li Zemin
 * @since 2024/5/14 17:04
 */
public class OperationFactory {

	public static Operation createOperate(String operate){
		Operation oper = null;
		switch (operate){
			case "+":
				oper = new OperationAdd();
				break;
			case "-":
				oper = new OperationSub();
				break;
			case "*":
				oper = new OperationMul();
				break;
			case "/":
				oper = new OperationDiv();
				break;
		}
		return oper;
	}

}
