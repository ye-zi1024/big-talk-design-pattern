package top.benbenye.big.talkdesign.pattern.observer;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Li Zemin
 * @since 2024/5/31 11:26
 */
public abstract class Subject {
	private List<Observer> observers = new ArrayList<>();

	public void attach(Observer observer) {
		observers.add(observer);
	}

	public void detach(Observer observer) {
		observers.remove(observer);
	}

	public void notifys() {
		for (Observer observer : observers) {
			observer.update();
		}
	}


}
