package top.benbenye.big.talkdesign.pattern.observer;

/**
 * @author Li Zemin
 * @since 2024/5/31 11:27
 */
public abstract class Observer {

	public abstract void update();
}
