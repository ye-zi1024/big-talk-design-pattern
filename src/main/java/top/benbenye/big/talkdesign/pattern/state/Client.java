package top.benbenye.big.talkdesign.pattern.state;

/**
 * @author Li Zemin
 * @since 2024/6/7 15:15
 */
public class Client {

	public static void main(String[] args) {
		Context context = new Context(new ConcreteStateA());
		context.request();
		context.request();
		context.request();
		context.request();
	}
}
