package top.benbenye.big.talkdesign.pattern.memento.first;

import lombok.Data;

/**
 * 备忘录.
 *
 * @author Li Zemin
 * @since 2024/6/11 10:33
 */
@Data
public class Memento {

	private String state;

	public Memento(String state) {
		this.state = state;
	}

}
