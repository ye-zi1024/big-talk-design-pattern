package top.benbenye.big.talkdesign.pattern.factory.simple;

/**
 * 简单工厂模式.
 *
 * @author Li Zemin
 * @since 2024/5/14 16:56
 */
public class FactoryPattern {

	public static void main(String[] args) {
		Operation oper = null;
		oper = OperationFactory.createOperate("+");
		oper.setNumberA(1d);
		oper.setNumberB(2d);
		double result = oper.getResult();
		System.out.println("result = " + result);

		oper = OperationFactory.createOperate("-");
		oper.setNumberA(1d);
		oper.setNumberB(2d);
		double result1 = oper.getResult();
		System.out.println("result1 = " + result1);

		oper = OperationFactory.createOperate("*");
		oper.setNumberA(1d);
		oper.setNumberB(2d);
		double result2 = oper.getResult();
		System.out.println("result2 = " + result2);

		oper = OperationFactory.createOperate("/");
		oper.setNumberA(1d);
		oper.setNumberB(2d);
		double result3 = oper.getResult();
		System.out.println("result3 = " + result3);
	}

}
