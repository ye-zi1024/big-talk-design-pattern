package top.benbenye.big.talkdesign.pattern.factory.simple;

import lombok.Getter;
import lombok.Setter;

/**
 * @author Li Zemin
 * @since 2024/5/14 17:05
 */
@Getter
@Setter
public abstract class Operation {

	private double numberA;
	private double numberB;

	public double getResult() {
		return 0d;
	}
}
