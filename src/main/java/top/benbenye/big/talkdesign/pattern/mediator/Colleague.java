package top.benbenye.big.talkdesign.pattern.mediator;

/**
 * 抽象同事类.
 *
 * @author Li Zemin
 * @since 2024/6/12 17:09
 */
public abstract class Colleague {

	protected Mediator mediator;

	public Colleague(Mediator mediator) {
		this.mediator = mediator;
	}

}
