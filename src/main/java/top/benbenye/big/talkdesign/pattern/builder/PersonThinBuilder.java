package top.benbenye.big.talkdesign.pattern.builder;

import java.awt.*;

/**
 * @author Li Zemin
 * @since 2024/5/29 9:26
 */
public class PersonThinBuilder extends PersonBuilder {

	public PersonThinBuilder(Graphics g, Color c) {
		super(g, c);
		this.g.setColor(c);
	}

	@Override
	public void buildHead() {
		g.drawRoundRect(50,20,30,30,30,30);
	}

	@Override
	public void buildBody() {
		g.drawRoundRect(60,50,10,50,0,0);
	}

	@Override
	public void buildArmLeft() {
		g.drawLine(60,50,40,100);
	}

	@Override
	public void buildArmRight() {
		g.drawLine(70,50,90,100);
	}

	@Override
	public void buildLegLeft() {
		g.drawLine(60,100,45,150);
	}

	@Override
	public void buildLegRight() {
		g.drawLine(70,100,85,150);
	}
}
