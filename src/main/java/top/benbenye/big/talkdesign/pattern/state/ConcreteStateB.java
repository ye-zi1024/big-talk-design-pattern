package top.benbenye.big.talkdesign.pattern.state;

/**
 * @author Li Zemin
 * @since 2024/6/7 15:13
 */
public class ConcreteStateB extends State {
	@Override
	public void handle(Context context) {
		context.setState(new ConcreteStateA());
	}
}
