package top.benbenye.big.talkdesign.pattern.adapter;

/**
 * @author Li Zemin
 * @since 2024/6/11 9:42
 */
public class Adaptee {

	public void SpecificRequest(){
		System.out.println("特殊的请求！");
	}

}
