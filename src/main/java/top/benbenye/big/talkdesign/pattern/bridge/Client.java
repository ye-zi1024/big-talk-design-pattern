package top.benbenye.big.talkdesign.pattern.bridge;

/**
 * @author Li Zemin
 * @since 2024/6/12 15:34
 */
public class Client {

	public static void main(String[] args) {

		Abstraction abstraction = new RefinedAbstraction();
		abstraction.setImplementor(new ConcreteImplementorA());
		abstraction.operation();

		abstraction.setImplementor(new ConcreteImplementorB());
		abstraction.operation();

	}
}
