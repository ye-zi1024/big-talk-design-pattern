package top.benbenye.big.talkdesign.pattern.bridge;

/**
 * @author Li Zemin
 * @since 2024/6/12 15:30
 */
public class Abstraction {

	protected Implementor implementor;

	public void setImplementor(Implementor implementor) {
		this.implementor = implementor;
	}

	public void operation() {
		implementor.operation();
	}

}
