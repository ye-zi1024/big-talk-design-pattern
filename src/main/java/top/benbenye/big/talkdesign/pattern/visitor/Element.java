package top.benbenye.big.talkdesign.pattern.visitor;

public abstract class Element {

  public abstract void accept(Visitor visitor);
}
