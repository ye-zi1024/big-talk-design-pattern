package top.benbenye.big.talkdesign.pattern.visitor;

public class ConcreteVisitor2 extends Visitor{

  @Override
  public void VisitConcreteElementA(ConcreteElementA concreteElementA) {
    System.out.println(concreteElementA.getName() + "被" + this.getTypeName() + "访问");
  }

  private String getTypeName() {
    return "ConcreteVisitor2";
  }

  @Override
  public void VisitConcreteElementB(ConcreteElementB concreteElementB) {
    System.out.println(concreteElementB.getName() + "被" + this.getTypeName() + "访问");
  }
}
