package top.benbenye.big.talkdesign.pattern.singleton;

/**
 * @author Li Zemin
 * @since 2024/6/12 10:43
 */
public class Client {

	public static void main(String[] args) {
		Singleton instance1 = Singleton.getInstance();
		Singleton instance2 = Singleton.getInstance();
		if (instance1 == instance2) {
			System.out.println("ok");
		}

		for (int i = 0; i < 100; i++) {
			System.out.println(i);
			Runnable runnable = () -> {
				Singleton instance = Singleton.getInstance();
				System.out.println(Thread.currentThread().getName() + " " + instance.toString());
			};
			runnable.run();
		}

	}
}
