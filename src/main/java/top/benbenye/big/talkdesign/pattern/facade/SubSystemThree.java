package top.benbenye.big.talkdesign.pattern.facade;

/**
 * @author Li Zemin
 * @since 2024/5/27 10:45
 */
public class SubSystemThree {
	public void methodC() {
		System.out.println("子系统方法三");
	}
}
