package top.benbenye.big.talkdesign.pattern.flyweight;

public abstract class FlyWeight {

  public abstract void operation(int extrinsicstate);

}
