package top.benbenye.big.talkdesign.pattern.builder;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

import sun.awt.image.BufImgSurfaceData;
import sun.java2d.SunGraphics2D;
import sun.java2d.SurfaceData;

/**
 * @author Li Zemin
 * @since 2024/5/29 9:34
 */
public class Client {
	public static void main(String[] args) throws IOException {
		BufferedImage bufferedImage = new BufferedImage(200, 200, BufferedImage.TYPE_INT_BGR);
		SurfaceData surfaceData = BufImgSurfaceData.createData(bufferedImage);
		Graphics graphics = new SunGraphics2D(surfaceData, Color.black, Color.white, null);
		PersonThinBuilder ptb = new PersonThinBuilder(graphics, Color.pink);
		PersonDirector pdThin = new PersonDirector(ptb);
		pdThin.createPerson();

		bufferedImage.flush();

		File file = new File("src/main/resources/personThinBuilder.jpg");
		ImageIO.write(bufferedImage, "jpg", file);
	}
}
