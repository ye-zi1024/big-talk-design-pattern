package top.benbenye.big.talkdesign.pattern.proxy;

/**
 * @author Li Zemin
 * @since 2024/5/24 11:13
 */
public interface GiveGift {
	void GiveDolls();

	void GiveFlowers();

	void GiveChocolate();
}
