package top.benbenye.big.talkdesign.pattern.composite;

import java.util.Arrays;
import java.util.stream.Collectors;

/**
 * @author Li Zemin
 * @since 2024/6/11 11:04
 */
public class Leaf extends Component {

	public Leaf(String name) {
		super.name = name;
	}

	@Override
	public void add(Component c) {
		System.out.println("cannot add to a leaf");
	}

	@Override
	public void remove(Component c) {
		System.out.println("cannot remove from a leaf");
	}

	@Override
	public void display(int depth) {
		System.out.println(String.join("", Arrays.stream(new String[depth]).map(e -> e == null ? "-" : e).collect(Collectors.toList())) + name);
	}

}
