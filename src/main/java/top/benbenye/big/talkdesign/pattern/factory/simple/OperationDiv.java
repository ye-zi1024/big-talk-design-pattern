package top.benbenye.big.talkdesign.pattern.factory.simple;

/**
 * @author Li Zemin
 * @since 2024/5/14 17:13
 */
public class OperationDiv extends Operation {
	@Override
	public double getResult() {
		return getNumberA() / getNumberB();
	}
}
