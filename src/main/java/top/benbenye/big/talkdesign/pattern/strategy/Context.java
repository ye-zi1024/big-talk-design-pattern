package top.benbenye.big.talkdesign.pattern.strategy;

/**
 * @author Li Zemin
 * @since 2024/5/20 8:19
 */
public class Context {

	Strategy strategy;

	public Context(Strategy strategy) {
		this.strategy = strategy;
	}

	public void ContextInterface() {
		strategy.AlgorithmInterface();
	}

}
