package top.benbenye.big.talkdesign.pattern.factory.abstrac;


/**
 * @author Li Zemin
 * @since 2024/6/7 14:38
 */
public interface IUser {
	void insert(User user);
	User getUser(int id);
}
