package top.benbenye.big.talkdesign.pattern.command;

/**
 * @author Li Zemin
 * @since 2024/6/12 15:54
 */
public class Client {
	public static void main(String[] args) {
		Receiver receiver = new Receiver();
		ConcreteCommand command = new ConcreteCommand(receiver);
		Invoker invoker = new Invoker(command);
		invoker.executeCommand();

	}
}
