package top.benbenye.big.talkdesign.pattern.composite;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author Li Zemin
 * @since 2024/6/11 11:08
 */
public class Composite extends Component {

	private List<Component> children = new ArrayList<Component>();

	public Composite(String name) {
		super.name = name;
	}

	@Override
	public void add(Component c) {
		children.add(c);
	}

	@Override
	public void remove(Component c) {
		children.remove(c);
	}

	@Override
	public void display(int depth) {
		System.out.println(String.join("",
				Arrays.stream(new String[depth]).map(e -> e == null ? "-" : e).collect(Collectors.toList())) + name);
		for (Component child : children) {
			child.display(depth + 2);
		}
	}

}
