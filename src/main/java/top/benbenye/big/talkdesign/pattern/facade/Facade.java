package top.benbenye.big.talkdesign.pattern.facade;

/**
 * @author Li Zemin
 * @since 2024/5/27 10:46
 */
public class Facade {
	SubSystemOne one;
	SubSystemTwo two;
	SubSystemThree three;
	SubSystemFour four;

	public Facade() {
		one = new SubSystemOne();
		two = new SubSystemTwo();
		three = new SubSystemThree();
		four = new SubSystemFour();
	}

	public void methodA() {
		System.out.println("方法组A() --- ");
		one.methodA();
		three.methodC();
		four.methodD();
	}

	public void methodB() {
		System.out.println("方法组B() --- ");
		two.methodB();
	}


}
