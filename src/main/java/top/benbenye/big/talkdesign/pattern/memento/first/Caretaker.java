package top.benbenye.big.talkdesign.pattern.memento.first;

import lombok.Data;

/**
 * 管理者.
 *
 * @author Li Zemin
 * @since 2024/6/11 10:34
 */
@Data
public class Caretaker {

	private Memento memento;

}
