package top.benbenye.big.talkdesign.pattern.proxy;

/**
 * @author Li Zemin
 * @since 2024/5/24 11:19
 */
public class Client {

	public static void main(String[] args) {
		SchoolGirl jiaojiao = new SchoolGirl();
		jiaojiao.setName("李娇娇");

		Proxy daili = new Proxy(jiaojiao);
		daili.GiveDolls();
		daili.GiveFlowers();
		daili.GiveChocolate();

	}
}
