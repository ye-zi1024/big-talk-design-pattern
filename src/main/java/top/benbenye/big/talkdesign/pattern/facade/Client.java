package top.benbenye.big.talkdesign.pattern.facade;

/**
 * @author Li Zemin
 * @since 2024/5/27 10:49
 */
public class Client {
	public static void main(String[] args) {
		Facade facade = new Facade();
		facade.methodA();
		facade.methodB();
	}
}
