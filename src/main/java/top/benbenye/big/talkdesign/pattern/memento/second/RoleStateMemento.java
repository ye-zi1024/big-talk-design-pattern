package top.benbenye.big.talkdesign.pattern.memento.second;

import lombok.Data;

/**
 * 备忘录.
 *
 * @author Li Zemin
 * @since 2024/6/11 10:48
 */
@Data
public class RoleStateMemento {

	// 生命力
	private int vit;

	// 攻击力
	private int atk;

	// 防御力
	private int def;

	public RoleStateMemento(int vit, int atk, int def) {
		this.vit = vit;
		this.atk = atk;
		this.def = def;
	}
}
