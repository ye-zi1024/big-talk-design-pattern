package top.benbenye.big.talkdesign.pattern.iterator;

import java.util.Arrays;

/**
 * @author Li Zemin
 * @since 2024/6/12 9:09
 */
public class ConcreteAggregate extends Aggregate {

	private Object[] items = new Object[10];

	@Override
	public Iterator createIterator() {
		return new ConcreteIterator(this);
	}

	public int count() {
		return items.length;
	}

	public Object get(int index) {
		return items[index];
	}

	public void set(int index, Object value) {
		items[index] = value;
	}

}
