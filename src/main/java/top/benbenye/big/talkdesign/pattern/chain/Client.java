package top.benbenye.big.talkdesign.pattern.chain;

/**
 * @author Li Zemin
 * @since 2024/6/12 16:53
 */
public class Client {

	public static void main(String[] args) {
		Handler h1 = new ConcreteHandler1();
		Handler h2 = new ConcreteHandler2();
		Handler h3 = new ConcreteHandler3();

		h1.setSuccessor(h2);
		h2.setSuccessor(h3);

		int[] requests = {2, 4, 12, 45, 67, 23, 18, 9};
		for (int request : requests) {
			h1.HandleRequest(request);
		}
	}
}
