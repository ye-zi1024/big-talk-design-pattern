package top.benbenye.big.talkdesign.pattern.state;

/**
 * @author Li Zemin
 * @since 2024/6/7 15:12
 */
public class ConcreteStateA extends State{
	@Override
	public void handle(Context context) {
		context.setState(new ConcreteStateB());
	}
}
