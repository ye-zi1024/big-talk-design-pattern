package top.benbenye.big.talkdesign.pattern.chain;

import java.sql.DriverPropertyInfo;

/**
 * @author Li Zemin
 * @since 2024/6/12 16:49
 */
public class ConcreteHandler1 extends Handler {
	@Override
	public void HandleRequest(int request) {
		if (request >= 0 && request < 10) {
			System.out.println(this.getType() + " 处理请求 " + request);
		} else if (successor != null) {
			successor.HandleRequest(request);
		}
	}

	private String getType() {
		return "张三";
	}
}
