package top.benbenye.big.talkdesign.pattern.adapter;

/**
 * @author Li Zemin
 * @since 2024/6/11 9:43
 */
public class Adapter extends Target {

	private Adaptee adaptee = new Adaptee();

	@Override
	public void Request() {
		adaptee.SpecificRequest();
	}
}
