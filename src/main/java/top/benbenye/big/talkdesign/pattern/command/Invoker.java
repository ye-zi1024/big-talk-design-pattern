package top.benbenye.big.talkdesign.pattern.command;

/**
 * @author Li Zemin
 * @since 2024/6/12 15:52
 */
public class Invoker {

	private Command command;

	public Invoker(Command command) {
		this.command = command;
	}

	public void executeCommand(){
		command.execute();
	}
}
