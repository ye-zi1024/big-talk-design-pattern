package top.benbenye.big.talkdesign.pattern.flyweight;

public class Client {

  public static void main(String[] args) {
    int extrinsicstate = 22;
    FlyWeightFactory flyWeightFactory = new FlyWeightFactory();
    FlyWeight x = flyWeightFactory.getFlyWeight("x");
    x.operation(--extrinsicstate);

    FlyWeight y = flyWeightFactory.getFlyWeight("y");
    y.operation(--extrinsicstate);

    FlyWeight z = flyWeightFactory.getFlyWeight("z");
    z.operation(--extrinsicstate);

    UnsharedConcreteFlyWeight unsharedConcreteFlyWeight = new UnsharedConcreteFlyWeight();
    unsharedConcreteFlyWeight.operation(--extrinsicstate);

  }

}
