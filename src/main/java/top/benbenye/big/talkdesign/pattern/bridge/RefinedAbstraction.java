package top.benbenye.big.talkdesign.pattern.bridge;

/**
 * @author Li Zemin
 * @since 2024/6/12 15:33
 */
public class RefinedAbstraction extends Abstraction {

	@Override
	public void operation() {
		super.operation();
	}
}
