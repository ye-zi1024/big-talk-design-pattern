package top.benbenye.big.talkdesign.pattern.interpreter;

/**
 * 抽象表达式.
 */
public abstract class AbstractExpression {

  public abstract void interpret(Context context);
}
