package top.benbenye.big.talkdesign.pattern.factory.abstrac;

/**
 * @author Li Zemin
 * @since 2024/6/7 14:42
 */
public class DB2User implements IUser {
	@Override
	public void insert(User user) {
		System.out.println("insert DB2User");
	}

	@Override
	public User getUser(int id) {
		return new User();
	}
}
