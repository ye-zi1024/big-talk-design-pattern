package top.benbenye.big.talkdesign.pattern.factory.method;

/**
 * @author Li Zemin
 * @since 2024/5/24 14:38
 */
public class LeiFeng {

	public void Sweep(){
		System.out.println("扫地");
	}

	public void Wash(){
		System.out.println("洗衣");
	}

	public void BuyRice(){
		System.out.println("买米");
	}

}
