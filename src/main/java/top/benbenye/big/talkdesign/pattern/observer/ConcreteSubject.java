package top.benbenye.big.talkdesign.pattern.observer;

import lombok.Data;

/**
 * @author Li Zemin
 * @since 2024/5/31 11:30
 */
@Data
public class ConcreteSubject extends Subject {

	private String subjectState;
}
