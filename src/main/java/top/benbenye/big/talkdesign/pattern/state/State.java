package top.benbenye.big.talkdesign.pattern.state;

/**
 * @author Li Zemin
 * @since 2024/6/7 15:09
 */
public abstract class State {

	public abstract void handle(Context context);

}
