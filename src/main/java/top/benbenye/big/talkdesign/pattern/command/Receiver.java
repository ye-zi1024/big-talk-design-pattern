package top.benbenye.big.talkdesign.pattern.command;

/**
 * @author Li Zemin
 * @since 2024/6/12 15:53
 */
public class Receiver {

	public void action(){
		System.out.println("执行请求！");
	}
}
