package top.benbenye.big.talkdesign.pattern.memento.second;

/**
 * @author Li Zemin
 * @since 2024/6/11 10:50
 */
public class Client {
	public static void main(String[] args) {
		// 大战BOSS前
		GameRole lixiaoyao = new GameRole();
		lixiaoyao.getInitState();
		lixiaoyao.stateDisplay();

		// 保存进度
		RoleStateCaretaker caretaker = new RoleStateCaretaker();
		caretaker.setMemento(lixiaoyao.saveState());

		// 大战BOSS时，损耗严重
		lixiaoyao.Fight();
		lixiaoyao.stateDisplay();

		// 恢复之前的状态
		lixiaoyao.recoveryState(caretaker.getMemento());

		lixiaoyao.stateDisplay();

	}

}
