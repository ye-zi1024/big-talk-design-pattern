package top.benbenye.big.talkdesign.pattern.proxy;

import lombok.Data;

/**
 * @author Li Zemin
 * @since 2024/5/24 11:15
 */
@Data
public class SchoolGirl {

	private String name;

}
