package top.benbenye.big.talkdesign.pattern.composite;

import lombok.Data;

/**
 * @author Li Zemin
 * @since 2024/6/11 11:02
 */
@Data
public abstract class Component {

	protected String name;

	public abstract void add(Component c);

	public abstract void remove(Component c);

	public abstract void display(int depth);


}
