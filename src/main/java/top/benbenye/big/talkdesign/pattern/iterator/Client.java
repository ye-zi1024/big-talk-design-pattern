package top.benbenye.big.talkdesign.pattern.iterator;

/**
 * @author Li Zemin
 * @since 2024/6/12 9:19
 */
public class Client {

	public static void main(String[] args) {
		ConcreteAggregate aggregate = new ConcreteAggregate();
		aggregate.set(0, "张三");
		aggregate.set(1, "李四");
		aggregate.set(2, "王五");
		aggregate.set(3, "赵六");
		aggregate.set(4, "守护");
		aggregate.set(5, "叶子");
		aggregate.set(6, "鲨鱼");

		Iterator iterator = new ConcreteIterator(aggregate);
		Object first = iterator.first();
		System.out.println("first = " + first);
		while (!iterator.isdone()){
			System.out.println(iterator.currentItem() + " 请先买车票");
			iterator.next();
		}


	}
}
