package top.benbenye.big.talkdesign.pattern.interpreter;

import lombok.Data;

@Data
public class Context {

  private String input;
  private String output;

}
