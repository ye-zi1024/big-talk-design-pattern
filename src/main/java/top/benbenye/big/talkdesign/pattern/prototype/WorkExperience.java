package top.benbenye.big.talkdesign.pattern.prototype;

import lombok.Data;

/**
 * @author Li Zemin
 * @since 2024/5/24 15:03
 */
@Data
public class WorkExperience implements Cloneable{

	private String workDate;
	private String company;


	@Override
	public WorkExperience clone() {
		try {
			WorkExperience clone = (WorkExperience) super.clone();
			// TODO: copy mutable state here, so the clone can't change the internals of the original
			return clone;
		} catch (CloneNotSupportedException e) {
			throw new AssertionError();
		}
	}
}
