package top.benbenye.big.talkdesign.pattern.adapter;

/**
 * @author Li Zemin
 * @since 2024/6/11 9:45
 */
public class Client {

	public static void main(String[] args) {
		Target adapter = new Adapter();
		adapter.Request();
	}

}
