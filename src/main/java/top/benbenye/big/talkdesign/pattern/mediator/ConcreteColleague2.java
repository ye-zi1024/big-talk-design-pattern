package top.benbenye.big.talkdesign.pattern.mediator;

/**
 * @author Li Zemin
 * @since 2024/6/12 17:13
 */
public class ConcreteColleague2 extends Colleague {

	public ConcreteColleague2(Mediator mediator) {
		super(mediator);
	}

	public void send(String message) {
		mediator.send(message, this);
	}

	public void notify(String message) {
		System.out.println("同事2得到消息：" + message);
	}
}
