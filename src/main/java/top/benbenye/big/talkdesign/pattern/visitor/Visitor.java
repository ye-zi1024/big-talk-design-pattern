package top.benbenye.big.talkdesign.pattern.visitor;

public abstract class Visitor {

  public abstract void VisitConcreteElementA(ConcreteElementA concreteElementA);
  public abstract void VisitConcreteElementB(ConcreteElementB concreteElementB);

}
