package top.benbenye.big.talkdesign.pattern.builder;

/**
 * @author Li Zemin
 * @since 2024/5/29 9:32
 */
public class PersonDirector {

	private PersonBuilder pb;

	public PersonDirector(PersonBuilder pb) {
		this.pb = pb;
	}

	public void createPerson(){
		pb.buildHead();
		pb.buildBody();
		pb.buildArmLeft();
		pb.buildArmRight();
		pb.buildLegLeft();
		pb.buildLegRight();
	}
}
