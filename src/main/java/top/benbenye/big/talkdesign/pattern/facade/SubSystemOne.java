package top.benbenye.big.talkdesign.pattern.facade;

/**
 * @author Li Zemin
 * @since 2024/5/27 10:45
 */
public class SubSystemOne {
	public void methodA() {
		System.out.println("子系统方法一");
	}
}
