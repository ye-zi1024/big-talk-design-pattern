package top.benbenye.big.talkdesign.pattern.chain;

/**
 * @author Li Zemin
 * @since 2024/6/12 16:47
 */
public abstract class Handler {

	protected Handler successor;

	public void setSuccessor(Handler successor) {
		this.successor = successor;
	}

	public abstract void HandleRequest(int request);

}
