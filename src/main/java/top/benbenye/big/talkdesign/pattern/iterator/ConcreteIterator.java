package top.benbenye.big.talkdesign.pattern.iterator;

/**
 * 迭代器实现类.
 *
 * @author Li Zemin
 * @since 2024/6/12 9:05
 */
public class ConcreteIterator extends Iterator {

	private ConcreteAggregate aggregate;
	private int current = 0;

	public ConcreteIterator(ConcreteAggregate aggregate) {
		this.aggregate = aggregate;
	}

	@Override
	public Object first() {
		return aggregate.get(0);
	}

	@Override
	public Object next() {
		Object ret = null;
		current++;
		if (current < aggregate.count()){
			ret = aggregate.get(current);
		}
		return ret;
	}

	@Override
	public Boolean isdone() {
		return aggregate.get(current) == null;
	}

	@Override
	public Object currentItem() {
		return aggregate.get(current);
	}
}
