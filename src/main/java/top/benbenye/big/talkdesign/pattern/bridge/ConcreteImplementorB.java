package top.benbenye.big.talkdesign.pattern.bridge;

/**
 * @author Li Zemin
 * @since 2024/6/12 15:28
 */
public class ConcreteImplementorB extends Implementor {
	@Override
	public void operation() {
		System.out.println("具体实现B的方法执行");
	}
}
