package top.benbenye.big.talkdesign.pattern.template.method;

/**
 * @author Li Zemin
 * @since 2024/5/24 15:33
 */
public class Client {
	public static void main(String[] args) {
		AbstractClass c;

		c = new ConcreteClassA();
		c.templateMethod();

		c = new ConcreteClassB();
		c.templateMethod();

	}
}
