package top.benbenye.big.talkdesign.pattern.decorator;

/**
 * @author Li Zemin
 * @since 2024/5/21 16:37
 */
public class ConcreteDecoratorB extends Decorator {
	private String addedState;

	@Override
	public void Operation() {
		super.Operation();
		addedBehavior();
		System.out.println("具体装饰对象B的操作");
	}

	private void addedBehavior(){

	}

}
