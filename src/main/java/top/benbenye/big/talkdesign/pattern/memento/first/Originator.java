package top.benbenye.big.talkdesign.pattern.memento.first;

import lombok.Data;

/**
 * 发起人.
 *
 * @author Li Zemin
 * @since 2024/6/11 10:31
 */
@Data
public class Originator {

	private String state;

	public Memento createMemento() {
		return new Memento(state);
	}

	public void setMemento(Memento memento) {
		state = memento.getState();
	}

	public void show() {
		System.out.println("state = " + state);
	}

}
