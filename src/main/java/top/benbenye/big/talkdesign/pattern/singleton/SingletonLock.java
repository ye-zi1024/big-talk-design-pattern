
package top.benbenye.big.talkdesign.pattern.singleton;

/**
 * @author Li Zemin
 * @since 2024/6/12 10:40
 */
public class SingletonLock {

	private static SingletonLock singleton = null;

	private static final String readOnly = "readOnly";

	private SingletonLock() {
	}

	public static SingletonLock getInstance() {
		if (singleton == null) {
			synchronized (readOnly) {
				if (singleton == null) {
					singleton = new SingletonLock();
				}
			}
		}
		return singleton;
	}
}
