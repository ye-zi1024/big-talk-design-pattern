package top.benbenye.big.talkdesign.pattern.decorator;

/**
 * @author Li Zemin
 * @since 2024/5/21 16:34
 */
public class ConcreteComponent extends Component {
	@Override
	public void Operation() {
		System.out.println("具体对象的操作");
	}
}
