package top.benbenye.big.talkdesign.pattern.factory.method;

/**
 * @author Li Zemin
 * @since 2024/5/24 14:42
 */
public class Client {

	public static void main(String[] args) {
		IFactory factory = new UndergraduateFactory();
		LeiFeng leiFeng = factory.CreateLeiFeng();
		leiFeng.Sweep();
		leiFeng.Wash();
		leiFeng.BuyRice();
	}

}
