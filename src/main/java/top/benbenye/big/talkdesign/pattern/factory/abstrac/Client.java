package top.benbenye.big.talkdesign.pattern.factory.abstrac;

/**
 * @author Li Zemin
 * @since 2024/6/7 14:43
 */
public class Client {
	public static void main(String[] args) {
		IUser iUser = DataAccess.CreateUser();
		iUser.insert(new User());
	}
}
