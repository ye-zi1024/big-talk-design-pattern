package top.benbenye.big.talkdesign.pattern.observer;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

/**
 * @author Li Zemin
 * @since 2024/5/31 11:32
 */

public class ConcreteObserver extends Observer {


	private String name;
	private String observerState;
	@Getter
	@Setter
	private ConcreteSubject subject;

	public ConcreteObserver(String name, ConcreteSubject subject) {
		this.name = name;
		this.subject = subject;
	}

	@Override
	public void update() {
		observerState = subject.getSubjectState();
		System.out.println("观察者" + name + "的新状态是" + observerState);
	}

}
