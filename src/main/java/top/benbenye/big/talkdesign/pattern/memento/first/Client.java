package top.benbenye.big.talkdesign.pattern.memento.first;

/**
 * @author Li Zemin
 * @since 2024/6/11 10:35
 */
public class Client {
	public static void main(String[] args) {
		Originator originator = new Originator();
		originator.setState("On");
		originator.show();

		Caretaker caretaker = new Caretaker();
		caretaker.setMemento(originator.createMemento());

		originator.setState("Off");
		originator.show();

		originator.setMemento(caretaker.getMemento());
		originator.show();


	}
}
