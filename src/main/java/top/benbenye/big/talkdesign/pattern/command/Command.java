package top.benbenye.big.talkdesign.pattern.command;

/**
 * @author Li Zemin
 * @since 2024/6/12 15:51
 */
public abstract class Command {

	protected Receiver receiver;

	public Command(Receiver receiver) {
		this.receiver = receiver;
	}

	public abstract void execute();
}
