package top.benbenye.big.talkdesign.pattern.template.method;

/**
 * @author Li Zemin
 * @since 2024/5/24 15:32
 */
public class ConcreteClassB extends AbstractClass{
	@Override
	public void PrimitiveOperation1() {
		System.out.println("ConcreteClassB PrimitiveOperation1 ...");
	}

	@Override
	public void PrimitiveOperation2() {
		System.out.println("ConcreteClassB PrimitiveOperation2 ...");
	}
}
