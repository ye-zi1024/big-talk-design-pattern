package top.benbenye.big.talkdesign.pattern.memento.second;

import lombok.Data;

/**
 * 发起人.
 *
 * @author Li Zemin
 * @since 2024/6/11 10:06
 */
@Data
public class GameRole {

	// 生命力
	private int vit;

	// 攻击力
	private int atk;

	// 防御力
	private int def;

	// 状态显示
	public void stateDisplay() {
		System.out.println("当前角色状态：");
		System.out.println("体力：" + vit);
		System.out.println("攻击力：" + atk);
		System.out.println("防御力：" + def);
		System.out.println("");
	}

	public void getInitState() {
		this.vit = 100;
		this.atk = 100;
		this.def = 100;
	}

	public void Fight() {
		this.vit = 0;
		this.atk = 0;
		this.def = 0;
	}

	// 保存角色状态
	public RoleStateMemento saveState() {
		return new RoleStateMemento(vit, atk, def);
	}

	// 恢复角色状态
	public void recoveryState(RoleStateMemento roleStateMemento) {
		this.vit = roleStateMemento.getVit();
		this.atk = roleStateMemento.getAtk();
		this.def = roleStateMemento.getDef();
	}


}


