package top.benbenye.big.talkdesign.pattern.prototype;

/**
 * @author Li Zemin
 * @since 2024/5/24 14:59
 */
public class Client {

	public static void main(String[] args) {
		Resume resume = new Resume("张三");
		resume.SetPersonalInfo("男","20");
		resume.SetWorkExperience("1998-2000","xxx公司");

		Resume resume1 = resume.clone();
		resume1.SetWorkExperience("2001-2002","yyy公司");

		Resume resume2 = resume.clone();
		resume2.SetWorkExperience("2003-2004","zzz公司");

		resume.display();
		resume1.display();
		resume2.display();


	}

}
