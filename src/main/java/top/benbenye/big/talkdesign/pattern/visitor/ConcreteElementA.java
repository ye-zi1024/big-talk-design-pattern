package top.benbenye.big.talkdesign.pattern.visitor;

public class ConcreteElementA extends Element{

  @Override
  public void accept(Visitor visitor) {
    visitor.VisitConcreteElementA(this);
  }

  public String getName() {
    return "ConcreteElementA";
  }
}
