package top.benbenye.big.talkdesign.pattern.decorator;

/**
 * @author Li Zemin
 * @since 2024/5/21 16:37
 */
public class ConcreteDecoratorA extends Decorator {
	private String addedState;

	@Override
	public void Operation() {
		super.Operation();
		addedState = "New State";
		System.out.println("具体装饰对象A的操作");
	}

}
