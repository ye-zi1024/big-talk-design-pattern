package top.benbenye.big.talkdesign.pattern.decorator;

/**
 * @author Li Zemin
 * @since 2024/5/21 16:35
 */
public abstract class Decorator extends Component {
	protected Component component;

	public void setComponent(Component component) {
		this.component = component;
	}

	@Override
	public void Operation() {
		if (component != null) {
			component.Operation();
		}

	}
}
