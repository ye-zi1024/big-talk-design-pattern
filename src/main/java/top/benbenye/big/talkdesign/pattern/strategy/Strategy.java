package top.benbenye.big.talkdesign.pattern.strategy;

/**
 * @author Li Zemin
 * @since 2024/5/20 8:17
 */
public abstract class Strategy {

	public abstract void AlgorithmInterface();

}
