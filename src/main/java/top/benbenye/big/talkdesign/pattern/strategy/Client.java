package top.benbenye.big.talkdesign.pattern.strategy;

/**
 * @author Li Zemin
 * @since 2024/5/20 8:21
 */
public class Client {



	public static void main(String[] args) {
		Context context;

		context = new Context(new ConcrereStrategyA());
		context.ContextInterface();

		context = new Context(new ConcrereStrategyB());
		context.ContextInterface();

		context = new Context(new ConcrereStrategyC());
		context.ContextInterface();
	}

}
