package top.benbenye.big.talkdesign.pattern.template.method;

/**
 * @author Li Zemin
 * @since 2024/5/24 15:30
 */
public abstract class AbstractClass {

	public abstract void PrimitiveOperation1();
	public abstract void PrimitiveOperation2();

	public void templateMethod(){
		PrimitiveOperation1();
		PrimitiveOperation2();
		System.out.println("");
	}

}
