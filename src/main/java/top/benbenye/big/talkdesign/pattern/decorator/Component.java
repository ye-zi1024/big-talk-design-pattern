package top.benbenye.big.talkdesign.pattern.decorator;

/**
 * @author Li Zemin
 * @since 2024/5/21 16:33
 */
public abstract class Component {

	public abstract void Operation();

}
