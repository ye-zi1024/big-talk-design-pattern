package top.benbenye.big.talkdesign.pattern.adapter;

/**
 * @author Li Zemin
 * @since 2024/6/11 9:41
 */
public class Target {

	public void Request() {
		System.out.println("普通的请求！");
	}

}
