package top.benbenye.big.talkdesign;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BigTalkDesignPatternApplication {

	public static void main(String[] args) {
		SpringApplication.run(BigTalkDesignPatternApplication.class, args);
	}

}
