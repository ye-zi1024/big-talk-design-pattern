## 📚简介

`big-talk-design-pattern`是基于《大话设计模式》输出的一些Demo Code.

-------------------------------------------------------------------------------

## 目录结构

```text
├─big-talk-design-pattern（父POM： 项目依赖、modules组织）
│ ├─pattern
│ │ ├─factory
│ │ │ ├─simple 简单工厂模式
│ │ │ ├─method 工厂方法模式
│ │ │ ├─abstrac 抽象工厂模式
│ │ ├─strategy 策略模式
│ │ ├─* 单一职责原则(SRP)
│ │ ├─* 开放-封闭原则
│ │ ├─* 依赖倒转原则（里氏代换原则）
│ │ ├─decorator 装饰模式
│ │ ├─proxy 代理模式
│ │ ├─prototype 原型模式
│ │ ├─templateMethod 模板方法模式
│ │ ├─lod 迪米特法则
│ │ ├─facade 外观模式
│ │ ├─builder 建造者模式
│ │ ├─observer(Publish/Subscribe) 观察者模式 -> 委托
│ │ ├─state 状态模式
│ │ ├─adapter 状态模式
│ │ ├─memento 备忘录模式
│ │ ├─composite 组合模式
│ │ ├─iterator 迭代器模式
│ │ ├─singleton 单例模式
│ │ ├─bridge 桥接模式
│ │ ├─command 命令模式
│ │ ├─chain 职责链模式
│ │ ├─mediator 中介者模式
│ │ ├─flyweight 享元模式
│ │ ├─interpreter 解释器模式
│ │ ├─visitor 访问者模式
```
![1718197652749.jpg](src%2Fmain%2Fresources%2F1718197652749.jpg)



